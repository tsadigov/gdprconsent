@isTest
public with sharing class GdprControllerTest {
  public static testMethod void Test_getPermissions() {
    Test.startTest();
    System.assert(GdprController.getPermissions().size() > 0);
    Test.stopTest();
  }

  public static testMethod void Test_getContactList() {
    GdprConsent__c cons = new GdprConsent__c();
    insert cons;

    insert new List<Contact>{
      new Contact(
        GdprConsent__c = cons.Id,
        LastName = 'test A',
        Email = 'a@mail.com'
      ),
      new Contact(
        GdprConsent__c = cons.Id,
        LastName = 'test B',
        Email = 'b@mail.com'
      ),
      new Contact(
        GdprConsent__c = cons.Id,
        LastName = 'test C',
        Email = 'c@mail.com'
      )
    };
    Test.startTest();
    List<SObject> result_a = GdprController.getContactList('a@', true);
    System.assertEquals(result_a.size(), 1);

    List<SObject> result_mail = GdprController.getContactList('mail', true);
    System.assertEquals(result_mail.size(), 3);
    Test.stopTest();
  }
}
