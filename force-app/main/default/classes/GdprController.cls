public with sharing class GdprController {
  public GdprController() {
  }

  @AuraEnabled(cacheable=false)
  public static void savePermission(GdprConsent__c record) {
    system.debug(record);
    update record;
  }

  @AuraEnabled(cacheable=false)
  public static void mergePermissions(List<GdprConsent__c> permissions) {
    //TODO: will nead to decide on merge rule
    Id winnerOption = permissions[0].Id;

    Set<Id> consentIds = (new Map<Id, GdprConsent__c>(permissions)).keyset();
    consentIds.remove(winnerOption);

    List<List<SObject>> recordLists = new List<List<SObject>>();
    recordLists.add(
      [SELECT Id, GdprConsent__c FROM Lead WHERE GdprConsent__c IN :consentIds]
    );
    recordLists.add(
      [
        SELECT Id, GdprConsent__c
        FROM Contact
        WHERE GdprConsent__c IN :consentIds
      ]
    );

    for (List<Sobject> records : recordLists) {
      for (SObject obj : records) {
        obj.put('GdprConsent__c', winnerOption);
      }
      update records;
    }

    //clean up unused
    delete [SELECT Id FROM GdprConsent__c WHERE Id IN :consentIds];
  }

  static string searchLike;
  @AuraEnabled(cacheable=true)
  public static List<SObject> getContactList(
    string searchText,
    boolean emailOnly
  ) {
    List<SObject> result = new List<SObject>();

    if (!String.isBlank(searchText)) {
      searchLike = '%' + searchText + '%';

      List<string> permissionFields = new List<string>();
      List<FieldDTO> permissions = getPermissions();
      for (FieldDTO field : permissions) {
        permissionFields.Add('GdprConsent__r.' + field.Name);
      }

      result.addAll(
        Database.query(
          'SELECT Id, Name,EMail, Title, Phone, GdprConsent__c, ' +
          String.join(permissionFields, ',') +
          ' FROM Contact WHERE EMail LIKE :searchLike LIMIT 100'
        )
      );
      result.addAll(
        Database.query(
          'SELECT Id, Name,EMail, Title, Phone, GdprConsent__c, ' +
          String.join(permissionFields, ',') +
          ' FROM Lead WHERE EMail LIKE :searchLike LIMIT 100'
        )
      );
    }
    return result;
  }

  public class FieldDTO {
    @AuraEnabled
    public string Label { get; set; }
    @AuraEnabled
    public string Name { get; set; }
    public FieldDTO(string name, string label) {
      this.Name = name;
      this.Label = label;
    }
  }

  @AuraEnabled(cacheable=true)
  public static List<FieldDTO> getPermissions() {
    Map<String, Schema.SObjectField> allFields = SObjectType.GdprConsent__c.fields.getMap();
    List<FieldDTO> result = new List<FieldDTO>();
    for (Schema.SObjectField field : allFields.values()) {
      DescribeFieldResult describe = field.getDescribe();
      if (
        describe.isAccessible() &&
        describe.isUpdateable() &&
        describe.isCustom()
      ) {
        //&& describe.getType() == Schema.DisplayType.Combobox && !describe.isCalculated() ){

        result.Add(new FieldDTO(describe.getName(), describe.getLabel()));
      }
    }
    return result;
  }
}
