import { api, LightningElement } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";

import savePermission from "@salesforce/apex/GdprController.savePermission";

const DELAY = 1500;

export default class ConsentDetail extends LightningElement {
  @api record;
  @api permissions;

  toast(title, body) {
    const toast = new ShowToastEvent({
      title: title,
      message: body
    });
    this.dispatchEvent(toast);
  }

  doSave() {
    savePermission({ record: this.record })
      .then(() => this.toast("saved", ""))
      .catch((e) => this.toast("error", e));
  }

  handlePermissionChange(evt) {
    const dynamic = { ...this.record };
    const { field, value } = evt.detail;
    dynamic[field] = value;
    this.record = dynamic;

    // Debouncing this method: Do not actually invoke the method call as long as this function is
    // being called within a delay of DELAY.
    window.clearTimeout(this.delayTimeout);
    this.delayTimeout = setTimeout(() => {
      try {
        this.doSave();
      } catch (e) {
        this.toast("error", e);
      }
    }, DELAY);
  }
}
