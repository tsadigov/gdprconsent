import { api, LightningElement } from "lwc";

export default class PermissionToggle extends LightningElement {
  @api metadata;
  @api record;

  handlePermissionChange(evt) {
    evt.preventDefault();
    let changeEvent = new CustomEvent("permissionchange", {
      bubbles: true,
      detail: { field: this.metadata.Name, value: evt.target.checked }
    });
    this.dispatchEvent(changeEvent);
  }

  get isProvidedPermission() {
    return this.record[this.metadata.Name];
  }
}
