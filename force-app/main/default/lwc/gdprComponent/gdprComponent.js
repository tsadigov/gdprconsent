import { LightningElement, wire, api, track } from "lwc";
import getContactList from "@salesforce/apex/GdprController.getContactList";
import getPermissions from "@salesforce/apex/GdprController.getPermissions";
import mergePermissions from "@salesforce/apex/GdprController.mergePermissions";

/** The delay used when debouncing event handlers before a method call. */
const DELAY = 350;

export default class GdprComponent extends LightningElement {
  placeholder = "email@example.com";
  contacts;
  error;
  @wire(getPermissions)
  permissions;

  handleKeyChange(event) {
    // Debouncing this method: Do not actually invoke the method call as long as this function is
    // being called within a delay of DELAY.
    window.clearTimeout(this.delayTimeout);
    const searchText = event.target.value;
    // eslint-disable-next-line @lwc/lwc/no-async-operation
    this.delayTimeout = setTimeout(() => {
      try {
        debugger;
        getContactList({ searchText, emailOnly: true })
          .then((data) => {
            this.contacts = data;
          })
          .catch((e) => {
            this.error = e;
          });
      } catch (e) {
        this.error = e;
      }
    }, DELAY);
  }

  get hasContacts() {
    return this.contacts && this.contacts.length > 0;
  }

  handleMerge() {
    mergePermissions(this.permissions);
  }
}

/*
    @wire(findContacts, { searchKey: '$searchKey' })
    contacts;

    handleKeyChange(event) {
        // Debouncing this method: Do not update the reactive property as long as this function is
        // being called within a delay of DELAY. This is to avoid a very large number of Apex method calls.
        window.clearTimeout(this.delayTimeout);
        const searchKey = event.target.value;
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.delayTimeout = setTimeout(() => {
            this.searchKey = searchKey;
        }, DELAY);
    }
*/
