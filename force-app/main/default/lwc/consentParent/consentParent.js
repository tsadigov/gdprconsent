import { api, LightningElement, track } from "lwc";

export default class ConsentParent extends LightningElement {
  @api contactOrLead;
  @api permissions;
  @track isExpanded;

  get js() {
    return JSON.stringify(this.contactOrLead);
  }

  expandHandler() {
    this.isExpanded = !this.isExpanded;
  }
}
