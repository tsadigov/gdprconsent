//BUG: there is currently an edge case when owner and manager2 are same, can be handled with tweeking
trigger PermissionToRoleHierarchy on Account(after insert, after update) {
  // here creating 2 sharing records
  //change triggers for action are: record creation, owner change and manager lookup change

  //STAGE 1 gather information, figure out what changed and what consequences should be in place
  List<Account> ownerSignal = new List<Account>();

  List<Account> managerExitSignal = new List<Account>();
  List<Account> managerEnterSignal = new List<Account>();
  for (Account acc : Trigger.new) {
    boolean ownerChanged =
      Trigger.isInsert || Trigger.oldMap.get(acc.Id).OwnerId != acc.OwnerId;
    if (ownerChanged) {
      ownerSignal.Add(acc);
    }

    if (Trigger.IsInsert) {
      if (acc.Manager2__c != null) {
        managerEnterSignal.Add(acc);
      }
    } else {
      Account old = Trigger.oldMap.get(acc.Id);
      if (old.Manager2__c != acc.Manager2__c) {
        if (old.Manager2__c != null) {
          managerExitSignal.Add(old);
        }

        if (acc.Manager2__c != null) {
          managerEnterSignal.Add(acc);
        }
      }
    }
  }

  Map<Id, Group> rolesAndSbordinates = new Map<Id, Group>();
  for (Group grp : [
    SELECT Id, DeveloperName, RelatedId
    FROM Group
    WHERE Type = 'RoleAndSubordinates'
  ]) {
    rolesAndSbordinates.put(grp.RelatedId, grp);
  }

  Set<Id> referencedUserIds = new Set<Id>();
  for (Account acc : managerEnterSignal) {
    referencedUserIds.add(acc.Manager2__c);
  }

  for (Account acc : managerExitSignal) {
    referencedUserIds.add(acc.Manager2__c);
  }

  for (Account acc : ownerSignal) {
    referencedUserIds.add(acc.OwnerId);
  }

  Map<Id, User> referencedUsers = new Map<Id, User>(
    [SELECT Id, Email, UserRoleId FROM User WHERE Id IN :referencedUserIds]
  );

  //STAGE 2 create the patch to close the gap

  // share with the owners subordinates
  List<AccountShare> ownerShares = new List<AccountShare>();
  for (Account acc : ownerSignal) {
    User owner = referencedUsers.get(acc.OwnerId);
    if (owner.UserRoleId != null) {
      Group role = rolesAndSbordinates.get(owner.UserRoleId);

      ownerShares.Add(
        new AccountShare(
          AccountId = acc.Id,
          UserOrGroupId = role.Id,
          AccountAccessLevel = 'Edit',
          OpportunityAccessLevel = 'Edit'
        )
      );
    }
  }

  // unshare from previous Manager2. Should I ? need to discuss

  // share with new Manager2
  List<AccountShare> managerShares = new List<AccountShare>();
  for (Account acc : managerEnterSignal) {
    User manager2 = referencedUsers.get(acc.Manager2__c);
    if (manager2.UserRoleId != null) {
      Group role = rolesAndSbordinates.get(manager2.UserRoleId);

      managerShares.Add(
        new AccountShare(
          AccountId = acc.Id,
          UserOrGroupId = role.Id,
          AccountAccessLevel = 'Read',
          OpportunityAccessLevel = 'Read'
        )
      );
    }
  }

  // STAGE 3 commit changes, complete Unit Of Work

  //TODO: merge into 1 DB transaction
  insert ownerShares;
  insert managerShares;

}
